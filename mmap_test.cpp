#include <iostream>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <unordered_map>
#include <vector>
#include <optional>
#include <algorithm>
#include <sys/mman.h>
#include <string_view>
#include "mmap_file_reader.h"
#include "word_count_tools.h"
// #include 
// #include <>



constexpr int threads_count = 3;

int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cerr << "Usage: ./bin file" << std::endl;
        return 1;
    }
    const char* filename = argv[1];

    MMapedFileReader mmap_reader(filename);
    auto start_and_len = mmap_reader.get_start_and_len();
    const char* fileData = start_and_len.start;
    off_t fileSize = start_and_len.len;
    // std::cout << "len = " << fileSize << std::endl;
    
    // int words_count = 0;
    // for (const auto& [w, c] : counts) {
    //     words_count += c;
    // }
    
    do_and_print_freq(std::cout, std::move(counts));


    return 0;
}
