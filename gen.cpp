#include <fstream>
#include <random>
#include <iostream>
#include <cassert>
#include <sstream>

const char* USAGE = "Usage ./gen output_size";
int main(int argc, char* argv[]) {

    if (argc < 2) {
        std::cerr << USAGE  << std::endl;
        return 1;
    }
    std::stringstream ss(argv[1]);
    size_t total_wanted_len = 0;
    ss >> total_wanted_len;
    char c;
    while (ss >> c) {
        if (std::tolower(c) == 'k') {
            total_wanted_len *= 1024;
        } else if (std::tolower(c) == 'm') {
            total_wanted_len *= 1024 * 1024;
        } else if (std::tolower(c) == 'g') {
            total_wanted_len *= 1024 * 1024 * 1024;
        } else {
            std::cerr << "Error: Unrecognized: output_size.\n" << USAGE << std::endl;
        }

    }
    // int total_wanted_len = std::stoi(argv[1]);

    // if (argc < 6) {
    //     std::cout << "Usage ./gen chars_count sep_to_word_ration avg_len stddev_len output_filename\n";
    // }

    // size_t total_len = std::stoul(argv[1]);
    // double sep_to_word_ration = std::stod(argv[2]);
    // double avg_len = std::stod(argv[3]);
    // double stddev_len = std::stod(argv[4]);
    // double output = std::stod(argv[5]);
    // std::string fname(argv[6]);


    // std::random_device rd;

    // std::mt19937 gen(rd());

    // std::normal_distribution<int> dist(avg_len, stddev_len);

    
    std::string seps("\n,,33323423412349                 80192804--=-=-=-=-=-2410");
    std::vector<std::string> words;
    int max_word_len = 40;
    words.push_back("a");
    for (int i = 0; i != max_word_len; ++i) {
        words.push_back(words.back() + "A");
    }

    words.push_back("B");
    for (int i = 0; i != max_word_len; ++i) {
        words.push_back(words.back() + "b");
    }

    // for (int i = 0; i != words.size(); ++i) {
    //     std::cerr << i << " " << words[i].size() << std::endl;
    // }
   
    std::random_device rd;  // a seed source for the random number engine
    std::mt19937 gen(rd()); // mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<size_t> distrib(0, words.size() - 1);


    
    size_t len = 0;
    while (len < total_wanted_len) {
        std::string sep( "1 2");
        sep += seps[distrib(gen) % seps.size()];
        size_t w_i = distrib(gen);
        size_t w_len = words[w_i].size();
        size_t rest = total_wanted_len - len;
        if (rest >= w_len) {
            std::cout << words[w_i];
            
        } else {
            std::cout << std::string(rest, '.');
            len += rest;
            break;
        }
        len += w_len;
        rest = total_wanted_len - len; 
        if ( rest >= sep.size()) {
            std::cout << sep;
            len += sep.size();
        } else {
            std::cout << std::string(rest, '=');
            len += rest;
            break;
        }
    }
    // std::cout << std::endl;

    // if (len != total_wanted_len) {
    //     std::cerr << "len = " << len << ", != total_wanted_len =" << total_wanted_len << std::endl;
    //     assert(len == total_wanted_len);
    // }

}