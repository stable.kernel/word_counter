#include <unordered_map>
#include <string>
// #include <iterator>
#include <fstream>
#include <iostream>
#include <memory>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <functional>
#include <algorithm>
#include <optional>
#include "word_count_tools.h"

// // all !std::isalpha -- are separators, reads string word by word and returns them lovercased
// struct WordIterator {
//     std::string buffer;

//     WordIterator() {

//     }

//     void set_string(std::string str) {
//         buffer = std::move(str);
//     }

//     bool has_more() {

//     }

//     std::string next_word() {

//     }
// };

// struct ChunkResult {
//     std::optional<std::string> prefix;
//     std::optional<std::string> suffix;
//     std::unordered_map<std::string, int> counts;
// };



std::istream& extract_word(std::istream& is, std::string& word) {
    word.clear();  // Clear the existing word

    // Skip non-alpha characters
    while (is && !std::isalpha(is.peek()))
        is.ignore();

    // Extract the word
    while (is && std::isalpha(is.peek()))
        word.push_back(std::tolower(is.get()));

    return is;
}

const std::string USAGE = "Usage: ./freq [input_file] [output_file]";


int main(int argc, char* argv[]) {
    // std::unique_ptr<std::istream> input;  // Smart pointer to input stream


    std::ifstream input(argv[1]);
    std::ofstream output("ouput.txt");
    std::unordered_map<std::string, int> counts;
    std::string line;
    while (std::getline(input, line)) {
        // std::cout << "line: " << line << std::endl;
        std::stringstream ss(line);
        std::string word;
        while (extract_word(ss, word)) {
            // std::cout << "word: " << word << std::endl;
            counts[word]++;
        }
    }

    int words_count = 0;
    for (const auto& [w, c] : counts) {
        words_count += c;
    }
    // std::cout << words_count << std::endl;
    // return 0;

    do_and_print_freq(std::cout, std::move(counts));
}