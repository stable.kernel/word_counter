#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <textfile>"
    exit 1
fi

textfile="$1"

cat "$textfile" | tr '[:upper:]' '[:lower:]' | tr -cs '[:alpha:]' '\n' | grep -v '^$' | sort | uniq -c | sort -nr
