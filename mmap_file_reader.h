#include <iostream>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <unordered_map>
#include <vector>
#include <optional>
#include <algorithm>
#include <sys/mman.h>
#include <string_view>


class MMapedFileReader {
    std::optional<struct stat> stats;
    std::optional<char*> start;
    int fd = -33;
public:
    struct StartAndLen {
        const char* start;
        off_t len;
    };
    MMapedFileReader(std::string_view fname) {
        fd = open(fname.data(), O_RDONLY);
        if (fd < 0) {
            throw std::runtime_error(std::string("Failed to open the file for reading <") + fname.data() + '>');
        }

        struct stat stats_loc;
        if (fstat(fd, &stats_loc) < 0) {
            throw std::runtime_error(std::string("Failed to get file size of <") + fname.data() + '>');          
        }

        stats = stats_loc;

        off_t fileSize = stats_loc.st_size;
        // std::cerr << "size=" << fileSize << std::endl;

        // Memory-map the file
        char* fileData = static_cast<char*>(mmap(NULL, fileSize, PROT_READ, MAP_PRIVATE, fd, 0));
        if (fileData == MAP_FAILED) {
            throw std::runtime_error(std::string("Failed to memory-map the file.") + fname.data() + '>');
        }

        start = fileData;
    }

    StartAndLen get_start_and_len() {
        return StartAndLen{*start, stats->st_size};
    }
    
    const char* get_start() const {
        return *start;
    }

    size_t get_len() const {
        return stats->st_size;
    }

    ~MMapedFileReader() {
        // Clean up: Unmap the file and close the file descriptor
        if (start) {
            munmap(*start, stats->st_size);
        }
        close(fd);
    }
};