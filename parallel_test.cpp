#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <thread>
#include <functional>

int calculateSumOfSquares(const std::vector<int>& range) {
    int sum = 0;
    for (int num : range) {
        sum += (num * num);
    }
    return sum;
}

int main() {
    std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int threads_number = 4;

    int range_size = v.size() / threads_number;

    std::vector<std::thread> threads;
    std::vector<int> results(threads_number);

    for (int i = 0; i < threads_number; ++i) {
        auto start = v.begin() + (i * range_size);
        auto end = (i == threads_number - 1) ? v.end() : start + range_size;

        threads.emplace_back([start, end, &results, i]() {
            std::vector<int> range(start, end);
            results[i] = calculateSumOfSquares(range);
        });
    }

    for (auto& thread : threads) {
        thread.join();
    }

    // Collect the results
    std::vector<int> answers = results;

    // Output the answers
    for (int answer : answers) {
        std::cout << answer << std::endl;
    }

    return 0;
}
