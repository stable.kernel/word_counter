#include "word_count_tools.h"
#include <map>
#include <functional>

void do_and_print_freq(std::ostream& output, std::unordered_map<std::string, int> counts) {
    std::map<int, std::vector<std::string>, std::greater<int>> freq;
    for (auto&& [w, c] : std::move(counts)) {
        freq[c].push_back(std::move(w));
    }

    for (auto& [c, words] : std::move(freq)) {
        std::sort(words.begin(), words.end());
        for (const auto& w : words) {
            output << c << " " << w << std::endl;
        }
    }
}

std::unordered_map<std::string, int> count_words_in_range(const char* input_begin, const char* input_end) {
    std::unordered_map<std::string, int> counts;
    std::string word;
    for (const char* p = input_begin; p != input_end; ++p) {
        // std::cout << "char=" << *p << std::endl;
        if (std::isalpha(*p)) {
            word += std::tolower(*p);
            
        } else {
            if (!word.empty()) {
                counts[word]++;
                word.clear();
            }
        }
    }
    if (!word.empty()) {
        counts[word]++;
    }
    return counts;
}

std::unordered_map<std::string, int> merge_counts(std::vector<std::unordered_map<std::string, int>> counts_vec) {
    auto counts = std::move(counts_vec[0]);
    for (int i = 1; i < counts_vec.size(); ++i) {
        for (auto [w, c] : std::move(counts_vec[i])) {
            counts[std::move(w)] += c;
        }
    }

    return counts;
}